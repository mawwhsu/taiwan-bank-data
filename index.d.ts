export interface BankData {
  code: string
  name: string
}

export interface BranchData extends BankData {
  /** BankData's code */
  bank: string
}

export const bankList: BankData[]
export const branchList: BranchData[]
