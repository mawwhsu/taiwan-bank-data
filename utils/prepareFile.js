const fs = require('fs')
const fetchBankData = require('./fetchBankData')
const {
  MAIN_FILE_NAME,
  ESM_FILE_NAME,
  EXPORTED_BANK_LIST_VAR_NAME,
  EXPORTED_BRANCH_LIST_VAR_NAME
} = require('./const')

async function prepareFile() {
  const { bankList, branchList } = await fetchBankData()
  fs.promises.writeFile(
    MAIN_FILE_NAME,
      `module.exports.${EXPORTED_BANK_LIST_VAR_NAME}=${JSON.stringify(bankList)};` + 
      `module.exports.${EXPORTED_BRANCH_LIST_VAR_NAME}=${JSON.stringify(branchList)};`
  )
  fs.promises.writeFile(
    ESM_FILE_NAME,
      `export * from './${MAIN_FILE_NAME}';`
  )
}

module.exports = prepareFile