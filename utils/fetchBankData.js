const axios = require('axios').default

/**
 * To check whether the table's schema meet expectation or not.
 * @param {string[]} titleRow 
 */
function checkTable(titleRow) {
  const [code, branchCode, bank, bankBranch] = titleRow
  if (
    code === '銀行代號' &&
    branchCode === '分支機構代號' &&
    bank === '金融機構名稱' &&
    bankBranch === '分支機構名稱'
  ) {
    return
  }

  throw new TypeError(`Table schema dose not meet expectation.`)
}

/**
 * @typedef {object} BankData
 * @property {string} code
 * @property {string} name
 * 
 * @typedef {BankData} BranchData
 * @property {string} bank BankData's code
 * 
 * @typedef {object} Result
 * @property {BankData[]} bankList
 * @property {BranchData[]} branchList
 * 
 * @returns {Promise<Result>}
 */
async function fetchBankData() {
  const response = await axios.get('https://www.fisc.com.tw/TC/OPENDATA/R2_Location.csv', {
    headers: {
      'content-type': 'text/plain;'
    }
  })

  const text = response.data
  const arr = text.split('\n')
  checkTable(arr.shift().split(','))

  const bankList = []
  const branchList = []
  arr.forEach(content => {
    if (!content) {
      return
    }

    const [code, branchCode, bank, bankBranch] = content.split(',')

    const bankData = {
      code,
      name: bank
    }
    if (bankList.findIndex(data => data.code === code) < 0) {
      bankList.push(bankData)
    }

    if (branchCode) {
      branchList.push({
        code: branchCode,
        name: bankBranch,
        bank: code
      })
    }
  })

  return {
    bankList,
    branchList
  }
}

module.exports = fetchBankData