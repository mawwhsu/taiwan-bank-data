const MAIN_FILE_NAME = 'index.js'
const ESM_FILE_NAME = 'index.esm.js'

const EXPORTED_BANK_LIST_VAR_NAME = 'bankList'
const EXPORTED_BRANCH_LIST_VAR_NAME = 'branchList'

module.exports = {
  MAIN_FILE_NAME,
  ESM_FILE_NAME,
  EXPORTED_BANK_LIST_VAR_NAME,
  EXPORTED_BRANCH_LIST_VAR_NAME
}