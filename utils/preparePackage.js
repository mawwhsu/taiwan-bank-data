const fs = require('fs')
const {
  MAIN_FILE_NAME,
  ESM_FILE_NAME
} = require('./const')

async function preparePackage() {
  const p = JSON.parse(await fs.promises.readFile('./package.json', 'utf-8'))

  const now = new Date()
  let [major, minor, patch] = p.version.split('.')
  const date = `${now.getFullYear()}${now.getMonth() + 1}${now.getDate()}`
  p.version = `${major}.${minor}.${patch}${date}`
  p.main = MAIN_FILE_NAME
  p.type = 'module'
  p.exports = {
    require: MAIN_FILE_NAME,
    import: ESM_FILE_NAME
  }

  fs.promises.writeFile('./package.json', JSON.stringify(p, null, '  '))
}

module.exports = preparePackage