const fs = require('fs')
const prepareFile = require('../utils/prepareFile')
const preparePackage = require('../utils/preparePackage')

const Env = process.env

async function prepareGitlab() {
  await preparePackage()
  const p = JSON.parse(await fs.promises.readFile('./package.json', 'utf-8'))
  const scope = `@${Env.CI_PROJECT_ROOT_NAMESPACE}`
  p.name = `${scope}/${Env.CI_PROJECT_NAME}`
  p.publishConfig = {
    [`${scope}:registry`]:
      `${Env.CI_SERVER_PROTOCOL}://${Env.PACKAGE_PUBLISH_DOMAIN}`
  }
  fs.promises.writeFile('./package.json', JSON.stringify(p, null, '  '))
}

prepareFile()
prepareGitlab()