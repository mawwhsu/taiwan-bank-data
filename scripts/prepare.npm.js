const prepareFile = require('../utils/prepareFile')
const preparePackage = require('../utils/preparePackage')

prepareFile()
preparePackage()