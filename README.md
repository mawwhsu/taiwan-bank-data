# Taiwan Bank Code

- ㄧ個簡單的 **台灣銀行代碼列表** 及 **支行代碼列表** package。
- 資料來源為 **財金資訊股份有限公司** 的 [開放資料](https://www.fisc.com.tw/TC/OPENDATA/R2_Location.csv)。
- 版本號的最末碼為 *patch 版本* 及 *數據更新日期* 的組合，例如 `1.2.1020191024` 代表：
  - patch 版本為 `10`。
  - package 中的銀行資訊為 `2019/10/24` 時從 [開放資料](https://www.fisc.com.tw/TC/OPENDATA/R2_Location.csv) 中抓取的。
  - 每個月的一號會透過 CI/CD 自動發佈新版，不論 [開放資料](https://www.fisc.com.tw/TC/OPENDATA/R2_Location.csv) 是否更新。

#### 台灣銀行代碼列表

```ts
import { bankList } from 'taiwan-bank-code'

const bank = bankList[0]
console.log({
  /** @type {string} */
  bankName: bank.name,
  /** @type {string} */
  bankCode: bank.code
})
```

#### 支行代碼列表

```ts
import { branchList } from 'taiwan-bank-code'

const branch = branchList[0]
console.log({
  /** @type {string} */
  branchName: branch.name,
  /** @type {string} */
  branchCode: branch.code,
  /** @type {string} */
  bank: branch.bank
})
```
